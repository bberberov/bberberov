<table cellpadding=0><tr>
<td valign=top width=50%>

### :wave:&emsp;Hello there

- :building_construction:&emsp;Currently working on:&emsp;:desktop_computer:&nbsp;homelab, :rocket:&nbsp;[Scorched3D], :fox_face:&nbsp;[Vulpes], :building_construction:&nbsp;[Autoinstalls]
- :book:&emsp;Currently learning:&emsp;:building_construction:&nbsp;[OpenStack](https://www.openstack.org/ '"The Most Widely Deployed Open Source Cloud Software in the World"')
- :handshake:&emsp;Looking to collaborate on:&emsp;:fox_face:&nbsp;[Vulpes], :rocket:&nbsp;[Scorched3D], [flashbench]
- :thinking:&emsp;Looking for help with:&emsp;:globe_with_meridians:&nbsp;WebDev
- :mailbox:&emsp;How to reach me:&emsp;:email:&nbsp;Emails, :speech_balloon:&nbsp;LinkedIn

> Hover over links to see more information

</td>
<td valign=top width=50%>

### :fox:&emsp;What's here

- :fox_face:&emsp;[Firefox Add-ons], get them from [AMO]
- :book:&emsp;Ebook soft-forks:&emsp;[LFS], [BLFS], [CLFS] and [CLFS Embedded]
- :building_construction:&emsp;[Autoinstalls] &ndash; for openSUSE, RHEL and Ubuntu
- :fox_face:&emsp;[Vulpes] &ndash; Experimental Firefox build
- :building_construction:&emsp;[multibuild] &ndash; tools for building on multiple distributions

</td>
</tr></table>

### :globe_with_meridians:&emsp;What's elsewhere

<table cellpadding=0><tr>
<td valign=top width=50%>

### :octopus:&emsp;[GitHub]

- :page_with_curl:&emsp;Dotfiles:&emsp;[userconfig], [userexec], and [userweb]
- :fork_and_knife:&emsp;Forks:&emsp; :rocket:&nbsp;[Scorched3D], [flashbench]
- :globe_with_meridians:&emsp;Web stuff:&emsp;[cydonia], [sunglasses]
- :scroll:&emsp;CV automation:&emsp;[xml-cv]
- :fork_and_knife:&emsp;Forks for preservation:&emsp;from :goggles:&nbsp;[OSVR], [XMind]
- [...](https://github.com/bberberov?tab=repositories)

</td>
<td valign=top width=50%>

### :factory:&emsp;[openSUSE Build Service]

- :package:&emsp;Builds for:&emsp;`LibreWolf`, `apkeep`, `gitahead`, `gittyup`, `hpn-ssh`, `miraclecast`, `tcc`, `uad-ng`, `vlang`, ...
- :package:&emsp;Targeting:&emsp;openSUSE, Fedora, Mageia, RHEL/EPEL, eventually Arch and Debian/Ubuntu
- :chart_with_upwards_trend:&emsp;[Multi-distro development repository status](https://build.opensuse.org/project/monitor/home:bberberov:develop:Multi)

</td>
</tr></table>

[AMO]:                    https://addons.mozilla.org/en-US/firefox/user/14697448/ 'Add-ons Mozilla.Org'
[openSUSE Build Service]: https://build.opensuse.org/project/show/home:bberberov  'My profile on openSUSE Build Service'

[GitHub]:     https://github.com/bberberov 'My profile on GitHub'
[OSVR]:       https://github.com/OSVR 'Open Source Virtual Reality'

[Scorched3D]: https://github.com/bberberov/scorched3d 'Scorched3D game, based on Scorched Earth'
[XMind]:      https://github.com/bberberov/xmind 'Mind-mapping tool'
[cydonia]:    https://github.com/bberberov/cydonia '"Utility" stylesheet'
[flashbench]: https://github.com/bberberov/flashbench 'Tool for analyzing flash block devices'
[sunglasses]: https://github.com/bberberov/sunglasses 'A theme for Pelican static-site generator'
[userconfig]: https://github.com/bberberov/userconfig 'Collection of configuration files and fragments'
[userexec]:   https://github.com/bberberov/userexec 'Collection of scripts and tools'
[userweb]:    https://github.com/bberberov/userweb 'Collection of userstyles and userscripts for use in browser extensions'
[xml-cv]:     https://github.com/bberberov/xml-cv 'CV and resume generating tools'

[GitLab]:          https://gitlab.com/bberberov 'My profile on GitLab'

[Autoinstalls]:    https://gitlab.com/clu-os/3rd/autoinstalls 'Automatic installation configuration files for various distributions'
[BLFS]:            https://gitlab.com/clu-os/docs/blfs 'Beyond Linux From Scratch book'
[CLFS Embedded]:   https://gitlab.com/clu-os/docs/cross-lfs-embedded 'Cross Linux From Scratch - Embedded book'
[CLFS]:            https://gitlab.com/clu-os/docs/cross-lfs 'Cross Linux From Scratch book'
[Firefox Add-ons]: https://gitlab.com/clu-os/firefox-add-ons
[LFS]:             https://gitlab.com/clu-os/docs/lfs 'Linux From Scratch book'
[Vulpes]:          https://gitlab.com/clu-os/software/vulpes 'Experimental web browser build, based on Firefox'
[multibuild]:      https://gitlab.com/clu-os/3rd/multibuild 'Tools for simplifying software builds for various distributions'
